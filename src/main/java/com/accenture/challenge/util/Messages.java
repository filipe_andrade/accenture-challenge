package com.accenture.challenge.util;

/**
 * Class to keep messages of application. On real applications, the messages should be in a
 * properties file and referenced by keys, but following the KISS principle, decided to keep
 * messages here.
 *
 * @author Filipe Andrade
 */
public class Messages {

  public static final String MAP_NOT_FOUND= "Map not found!!";
  public static final String MAP_NOT_SAVED = "Error on saving Map";
  public static final String ORIGIN_DESTINATION_INVALID = "Origin or Destination not found on routes";
  public static final String PATH_NOT_FOUND = "Path to destination not found";
  
}
