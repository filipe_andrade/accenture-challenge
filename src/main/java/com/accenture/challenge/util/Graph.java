package com.accenture.challenge.util;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Graph {

	private Map<String, Node> nodes = new HashMap<>();
	private Set<Edge> edges = new HashSet<>();

	public Map<String,Node> getNodes() {
		return nodes;
	}

	public void setNodes(Map<String,Node> nodes) {
		this.nodes = nodes;
	}

	public Set<Edge> getEdges() {
		return edges;
	}

	public void setEdges(Set<Edge> edges) {
		this.edges = edges;
	}
	
	public Node getNodeFromId(String id) {
		return this.nodes.get(id);
	}
}
