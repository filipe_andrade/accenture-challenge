package com.accenture.challenge.algorithms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
You are given a list of integers and a list of pairs of integers.
For each pair, you want to find the minimum distance at which the integers of the pair can be found in
the given list of integers.
Implement the method findMinimumDistances() so that it returns an array of integers with the
minimum distance between each pair.
This array must have the distances in the same order of the given array of pairs.
If at least one of the integers in a pair is not present in the array of integers, the distance between
them must be -1.
Consider that distance is defined as |j - i| where 'i' and 'j' are the indexes of the integers in the array.
You can write any methods or classes you would like. However, all new classes must be written in
this
file and you cannot change the DistancePairNumbers class name or the findMinimumDistances()
method signature.
Example:
--------
Input:
integers: [9,1,6,2,7,1,9,7,3]
pairs: [[3,7],[1,2],[1,7],[9,6],[1,1],[9,2],[6,7],[2,7],[9,7],[9,5],[1,6]]
Output: [1,2,1,2,0,3,2,1,1,-1,1]
*/
public class DistancePairNumbers {

	/**
	 * Finds the minimum distance between the pairs in the array of integers.
	 * 
	 * @param integers
	 *            The array of integers.
	 * @param pairs
	 *            The array of pairs of integers.
	 * @return An array with the minimum distance of each pair.
	 */
	public Integer[] findMinimumDistances(Integer[] integers, Integer[][] pairs) {
		Map<Integer, List<Integer>> map = new HashMap<>();
		Integer[] result = new Integer[pairs.length];
		for (int i = 0; i < integers.length; i++) {
			List<Integer> list = map.getOrDefault(integers[i], new ArrayList<>());
			list.add(i);
			map.put(integers[i], list);
		}

		for (int index = 0; index < pairs.length; index++) {
			Integer[] pair = pairs[index];
			List<Integer> listX = map.get(pair[0]);
			List<Integer> listY = map.get(pair[1]);
			if (listX == null || listY == null) {
				result[index] = -1;
			} else if (pair[0] == pair[1]) {
				result[index] = 0;
			} else {
				result[index] = findMinimum(listX, listY);
			}

		}

		return result;
	}

	private int findMinimum(List<Integer> listX, List<Integer> listY) {
		int minimum = Integer.MAX_VALUE;
		for (int i = 0; i < listX.size(); i++) {
			int valX = listX.get(i);
			for (int j = 0; j < listY.size(); j++) {
				int valY = listY.get(j);
				int diff = Math.abs(valX - valY);
				minimum = Math.min(minimum, diff);
				if(minimum == 1) { //minimum possible when numbers are different
					return minimum;
				}
			}
		}
		return minimum;
	}

}
