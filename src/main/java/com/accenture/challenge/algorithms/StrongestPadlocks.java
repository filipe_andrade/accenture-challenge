package com.accenture.challenge.algorithms;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

/*
A company has 'n' spots for people to park their bicycles.
You are responsible for locking up the arriving bicycles using padlocks.
You have, at your disposal, one box full of padlocks and another box full of keys.
Each padlock has a tag containing its numeric ID and its unique strength.
Each key has a tag containing the numeric ID of the padlock it opens.
You want to select the 'n' strongest padlocks for which you also have a key.
Implement the method findStrongestPadlocks() so that it returns an array of integers with the 'n'
strongest padlocks.
You can write any methods or classes you would like. However, all new classes must be written in
this
file and you cannot change the StrongestPadlocks class name or the findStrongestPadlocks()
method signature.
Example:
--------
Input:
padlocks: [[4,11],[2,15],[5,16],[3,40],[1,20],[9,60],[6,22]]
keys: [2,6,7,3,4]
n: 3
Output: [3,6,2]
*/
public class StrongestPadlocks {
	/**
	 * Finds the 'n' strongest padlocks for which there is a key.
	 * 
	 * @param padlocks
	 *            The array of padlocks. Each padlock is represented by an array of
	 *            two integers; the first one is its ID and the second one is its
	 *            strength.
	 * @param keys
	 *            The array of keys. Each key is represented by the ID of the
	 *            padlock it opens.
	 * @param n
	 *            The number of spots available.
	 * @return An array with the IDs of the strongest padlocks.
	 */
	public Integer[] findStrongestPadlocks(Integer[][] padlocks, Integer[] keys, Integer n) {
		List<Padlock> pads = new ArrayList<>();
		List<Padlock> padsWithKeys = new ArrayList<>();
		for (Integer[] pad : padlocks) {
			pads.add(new Padlock(pad[0], pad[1]));
		}
		for (Integer key : keys) {
			padsWithKeys.add(new Padlock(key));
		}
		pads.retainAll(padsWithKeys);
		Stream<Padlock> sorted = pads.parallelStream().sorted((a, b) -> {
			return b.getStrength() - a.getStrength();
		});

		if (pads.size() > n) {
			sorted =  sorted.limit(n);
		} 
		return sorted.map( pad -> pad.getKey()).toArray(Integer[]::new);
	}

	public static void main(String[] args) {

		Integer[][] padlocks = new Integer[][] { { 4, 11 }, { 2, 15 }, { 5, 16 }, { 3, 40 }, { 1, 20 }, { 9, 60 },
				{ 6, 22 } };
		Integer[] keys = new Integer[] { 2, 6, 7, 3, 4 };
		StrongestPadlocks sp = new StrongestPadlocks();
		sp.findStrongestPadlocks(padlocks, keys, 3);

	}
}

class Padlock {
	Integer key;
	Integer strength;

	public Padlock(Integer key) {
		this.key = key;
	}

	public Padlock(Integer key, Integer strength) {
		this(key);
		this.strength = strength;
	}

	public Integer getKey() {
		return key;
	}

	public Integer getStrength() {
		return strength;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Padlock) {
			return this.getKey().equals(((Padlock) obj).getKey());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getKey();
	}
}
