package com.accenture.challenge.controller.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.challenge.controller.LogisticMapBC;
import com.accenture.challenge.exception.ChallengeException;
import com.accenture.challenge.exception.ResourceNotFoundException;
import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.model.Route;
import com.accenture.challenge.model.dto.PathRequestDTO;
import com.accenture.challenge.model.dto.PathResponseDTO;
import com.accenture.challenge.persistence.LogisticMapDAO;
import com.accenture.challenge.util.Edge;
import com.accenture.challenge.util.Graph;
import com.accenture.challenge.util.Messages;
import com.accenture.challenge.util.Node;
import com.accenture.challenge.util.PathAlgorithm;

@Controller
public class LogisticMapBCImpl implements LogisticMapBC {

	private LogisticMapDAO dao;

	@Autowired
	public LogisticMapBCImpl(LogisticMapDAO dao) {
		this.dao = dao;
	}

	@Override
	public LogisticMap load(Long id) {
		LogisticMap map = this.dao.load(id);
		if (map == null) {
			throw new ResourceNotFoundException(Messages.MAP_NOT_FOUND);
		}
		return map;
	}

	@Override
	@Transactional
	public Long save(LogisticMap map) {
		this.preInsert(map);
		Long id = this.dao.save(map);
		if (id == null) {
			throw new ChallengeException(Messages.MAP_NOT_SAVED);
		}
		return id;
	}
	
	/**
	 * Setting the map on route objects before saving
	 * @param map Map object with all routes
	 */
	private void preInsert(LogisticMap map) {
		if(map.getRoutes() != null) {
			map.getRoutes().forEach(route -> route.setMap(map));
		}
	}

	@Override
	public PathResponseDTO calculatePath(PathRequestDTO pathRequest) {
		LogisticMap map = this.dao.loadFirst();
		if(map != null) {
			Graph graph = buildGraph(map);
			PathAlgorithm pathAlgorithm = new PathAlgorithm(graph.getEdges());
			Node nodeOrigin = graph.getNodeFromId(pathRequest.getOrigin());
			Node nodeDestination = graph.getNodeFromId(pathRequest.getDestination());
			if(nodeOrigin == null || nodeDestination == null) {
				throw new ChallengeException(Messages.ORIGIN_DESTINATION_INVALID);
			}
			pathAlgorithm.execute(nodeOrigin);
			LinkedList<Node> path = pathAlgorithm.getPath(nodeDestination);
			if(path == null) {
				throw new ChallengeException(Messages.PATH_NOT_FOUND);
			}
			String stringPath = pathAlgorithm.getStringPath(path);
			Integer pathDistance = pathAlgorithm.getPathDistance(nodeDestination);
			return createPathResponse(stringPath, pathDistance, pathRequest);			
		}
		throw new ChallengeException(Messages.PATH_NOT_FOUND);
	}
	
	/**
	 * Create the graph representing the routes of the map
	 * @param map	The LogisticMap to build the graph
	 * @return Graph representing the routes
	 */
	private Graph buildGraph(LogisticMap map) {
		Graph graph = new Graph();
		HashMap<String, Node> hashMap = new HashMap<>();
		Set<Edge> edges = new HashSet<>();
		List<Route> routes = map.getRoutes();
		for(Route route: routes) {
			Node nodeOrigin = hashMap.getOrDefault(route.getOrigin(), new Node(route.getOrigin()));
			Node nodeDestination = hashMap.getOrDefault(route.getDestination(), new Node(route.getDestination()));
			
			hashMap.putIfAbsent(route.getOrigin(), nodeOrigin);
			hashMap.putIfAbsent(route.getDestination(), nodeDestination);
			
			edges.add(new Edge(route.getId(), nodeOrigin, nodeDestination, route.getDistance()));			
		}
		graph.setEdges(edges);
		graph.setNodes(hashMap);
		return graph;
	}
	
	/**
	 * Create the response of the Path Requested
	 * @param path String representing the path
	 * @param pathDistance Distance of the path
	 * @param pathRequest PathRequest object with useful information
	 * @return PathResponseDTO with information of requested path
	 */
	private PathResponseDTO createPathResponse(String path, Integer pathDistance,PathRequestDTO pathRequest) {
		PathResponseDTO pathResponse = new PathResponseDTO();
		pathResponse.setPath(path);		
		Double cost = ( Double.valueOf(pathDistance) /pathRequest.getAutonomy()) * pathRequest.getFuelPrice();
		pathResponse.setCost(cost);
	
		return pathResponse;
	}

}
