package com.accenture.challenge.controller;

import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.model.dto.PathRequestDTO;
import com.accenture.challenge.model.dto.PathResponseDTO;

public interface LogisticMapBC {

	/**
	 * Load a LogisticMap by Id
	 * @param id Id of LogisticMap
	 * @return LogisticMap of Id
	 */
	public LogisticMap load(Long id);

	/**
	 * Save a LogisticMap
	 * @param map Map to be saved
	 * @return Id of the new map saved
	 */
	public Long save(LogisticMap map);
	
	/**
	 * Method to calculate the better path and the cost of take it.
	 * @param pathRequest Object containing useful information like origin/destination of the 
	 * request path
	 * @return {@link PathResponseDTO} with information about the path and cost
	 */
	public PathResponseDTO calculatePath(PathRequestDTO pathRequest);

}
