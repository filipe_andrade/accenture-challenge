package com.accenture.challenge.model.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PathRequestDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@NotBlank
	private String origin;

	@NotNull
	private String destination;

	@NotNull
	private Double autonomy;

	@NotNull
	private Double fuelPrice;

	public PathRequestDTO() {
		super();
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Double getAutonomy() {
		return autonomy;
	}

	public void setAutonomy(Double autonomy) {
		this.autonomy = autonomy;
	}

	public Double getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(Double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

}
