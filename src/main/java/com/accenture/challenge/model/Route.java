package com.accenture.challenge.model;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.validator.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "route")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Route implements Serializable {

	private static final long serialVersionUID = 6906012651688235050L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@JsonBackReference
	@ManyToOne
	@JoinColumn(name = "map_id", nullable = false)
	private LogisticMap map;

	@NotBlank
	@Basic
	@Column(name = "origin", length = 10, nullable = false)
	private String origin;

	@NotBlank
	@Basic
	@Column(name = "destination", length = 10, nullable = false)
	private String destination;

	@NotNull
	@Basic
	@Column(name = "distance", nullable = false)
	private Integer distance;

	public Route() {
		super();
	}

	public Route(String origin, String destination, Integer distance) {
		super();
		this.origin = origin;
		this.destination = destination;
		this.distance = distance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public LogisticMap getMap() {
		return map;
	}

	public void setMap(LogisticMap map) {
		this.map = map;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Integer getDistance() {
		return distance;
	}

	public void setDistance(Integer distance) {
		this.distance = distance;
	}

}
