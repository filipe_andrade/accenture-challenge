package com.accenture.challenge.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "logistic_map")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class LogisticMap implements Serializable {

	private static final long serialVersionUID = 6906012651688235050L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", nullable = false)
	private Long id;

	@Basic
	@Column(name = "name", length = 100, nullable = false)
	private String name;

	@JsonManagedReference
	@OneToMany(mappedBy = "map", cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	private List<Route> routes;

	public LogisticMap() {
		super();
	}

	public LogisticMap(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Route> getRoutes() {
		return routes;
	}

	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}

	public void addRoute(Route route) {
		if (this.routes == null) {
			this.routes = new ArrayList<>();
		}
		this.routes.add(route);
	}

}
