package com.accenture.challenge.service;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

public abstract class GenericService {
	
	private Logger logger;
	
	public GenericService() {
		super();
		logger = Logger.getLogger(getClass().getName());
	}
	
	protected Response createResponseFromException(Exception e, Response.Status status) {
		getLogger().log(Level.FINE, e.getMessage(), e);
		return Response.status(status).entity(e.getMessage()).build();
	}
	
	protected Logger getLogger() {
		return logger;
	}

}
