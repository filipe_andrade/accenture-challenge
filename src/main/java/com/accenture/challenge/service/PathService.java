package com.accenture.challenge.service;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.accenture.challenge.controller.LogisticMapBC;
import com.accenture.challenge.exception.ChallengeException;
import com.accenture.challenge.model.dto.PathRequestDTO;
import com.accenture.challenge.model.dto.PathResponseDTO;

@Path("/path")
public class PathService extends GenericService {

	private LogisticMapBC controller;

	@Autowired
	public PathService(LogisticMapBC controller) {
		this.controller = controller;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response calculatePath(@Valid @RequestBody PathRequestDTO pathRequest) {
		Response response = null;
		try {
			PathResponseDTO responsePath = this.controller.calculatePath(pathRequest);
			response = Response.status(Response.Status.OK).entity(responsePath).build();
		} catch (ChallengeException e) {
			response = createResponseFromException(e, Response.Status.BAD_REQUEST);
		}
		return response;
	}

}
