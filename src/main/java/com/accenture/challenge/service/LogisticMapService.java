package com.accenture.challenge.service;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.accenture.challenge.controller.LogisticMapBC;
import com.accenture.challenge.exception.ChallengeException;
import com.accenture.challenge.exception.ResourceNotFoundException;
import com.accenture.challenge.model.LogisticMap;

@Path("/map")
public class LogisticMapService extends GenericService {

	private LogisticMapBC controller;

	@Autowired
	public LogisticMapService(LogisticMapBC controller) {
		this.controller = controller;
	}

	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getById(@PathParam("id") Long id) {
		Response response = null;
		try {
			LogisticMap map = this.controller.load(id);
			response = Response.status(Response.Status.OK).entity(map).build();
		} catch (ResourceNotFoundException e) {
			response = createResponseFromException(e, Response.Status.NOT_FOUND);
		}
		return response;
	}

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response insert(@Valid @RequestBody LogisticMap map) {
		Response response = null;
		try {
			Long id = this.controller.save(map);
			response = Response.status(Response.Status.CREATED).entity(id).build();
		} catch (ChallengeException e) {
			response = createResponseFromException(e, Response.Status.BAD_REQUEST);
		}
		return response;
	}

}
