package com.accenture.challenge.exception;

/**
 * Internal Exception to mark exception related to resources not found on database
 * 
 * @author Filipe Andrade
 *
 */
public class ResourceNotFoundException extends RuntimeException {

  private static final long serialVersionUID = -8442810153556844140L;

  public ResourceNotFoundException(String message) {
    super(message);
  }

}
