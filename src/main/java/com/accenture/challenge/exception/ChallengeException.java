package com.accenture.challenge.exception;

/**
 * Internal Exception to mark validation and other errors
 *
 * @author Filipe Andrade
 *
 */
public class ChallengeException extends RuntimeException {

  private static final long serialVersionUID = 5391138102573514632L;

  public ChallengeException(String message) {
    super(message);
  }

}
