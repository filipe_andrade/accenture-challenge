package com.accenture.challenge.persistence.impl;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.persistence.LogisticMapDAO;

@Repository
public class LogisticMapDAOImpl extends HibernateDaoSupport implements LogisticMapDAO {

	@Autowired
	public LogisticMapDAOImpl(SessionFactory sessionFactory) {
		setSessionFactory(sessionFactory);
	}

	@Override
	public LogisticMap load(Long id) {
		return getHibernateTemplate().get(LogisticMap.class, id);
	}

	@Override
	@Transactional(readOnly = false)
	public Long save(LogisticMap map) {
		return (Long) getHibernateTemplate().save(map);
	}

	@SuppressWarnings("unchecked")
	@Override
	public LogisticMap loadFirst() {
		DetachedCriteria criteria = DetachedCriteria.forClass(LogisticMap.class);
		List<LogisticMap> maps = (List<LogisticMap>) getHibernateTemplate().findByCriteria(criteria);
		if (maps.size() > 0) {
			LogisticMap map = maps.get(0);
			// return this.load(map.getId());
			return map;
		}
		return null;
	}

}
