package com.accenture.challenge.persistence;

import com.accenture.challenge.model.LogisticMap;

public interface LogisticMapDAO {

	/**
	 * Load map with id passed
	 * @param id Id of map you want to load
	 * @return LogisticMap from the id. Null if doesnt exists
	 */
	public LogisticMap load(Long id);

	/**
	 * Save the map on database
	 * @param map LogisticMap to be saved
	 * @return Id of the new map saved
	 */
	public Long save(LogisticMap map);
	
	/**
	 * Load the first map on database
	 * @return LogisticMap found
	 */
	public LogisticMap loadFirst();

}
