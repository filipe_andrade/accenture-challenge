package com.accenture.challenge.service;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.model.Route;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class IntegrationLogisticMapServiceTest {

	private static final String DEFAULT_PATH = "http://localhost:8080/rest/";
	private static final String MAP_PATH = "map/";

	private static HttpServer server;

	@BeforeClass
	public static void setup() {
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages("com.accenture.challenge");
		server = GrizzlyHttpServerFactory.createHttpServer(URI.create(DEFAULT_PATH), resourceConfig);
	}

	public static WebTarget createDefaultWebTarget() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(DEFAULT_PATH);
		webTarget.register(JacksonJaxbJsonProvider.class);
		return webTarget;
	}

	@AfterClass
	public static void tearDown() {
		server.shutdown();
	}

	@Test
	public void whenLoadInexistentThenNotFound() {
		WebTarget webTarget = createDefaultWebTarget().path(MAP_PATH).path("20");
		Builder request = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = request.get();
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void whenSaveValidThenReturnId() {
		LogisticMap map = this.createTestItem();

		WebTarget webTarget = createDefaultWebTarget().path(MAP_PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(map));
		Long newId = response.readEntity(Long.class);
		Assert.assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());
		Assert.assertNotNull(newId);
	}

	@Test
	public void whenSaveInvalidThenReturnBadRequest() {
		LogisticMap map = new LogisticMap();
		WebTarget webTarget = createDefaultWebTarget().path(MAP_PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(map));
		Assert.assertEquals(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
	}

	@Test
	public void whenLoadExistentThenReturnOK() {
		Long newId = createInsertDefaultTestOrder();

		WebTarget webTargetLoad = createDefaultWebTarget().path(MAP_PATH).path(String.valueOf(newId));
		Builder requestLoad = webTargetLoad.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response responseLoad = requestLoad.get();
		LogisticMap returned = responseLoad.readEntity(LogisticMap.class);

		Assert.assertEquals(Response.Status.OK.getStatusCode(), responseLoad.getStatus());
		Assert.assertNotNull(returned);
	}

	private Long createInsertDefaultTestOrder() {
		LogisticMap map = this.createTestItem();

		WebTarget webTarget = createDefaultWebTarget().path(MAP_PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(map));
		Long newId = response.readEntity(Long.class);
		return newId;
	}

	private LogisticMap createTestItem() {
		LogisticMap map = new LogisticMap(1l, "Testmap");
		map.addRoute(new Route("A", "B", 10));
		map.addRoute(new Route("B", "C", 5));
		return map;
	}

}
