package com.accenture.challenge.service;

import javax.ws.rs.core.Response;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.accenture.challenge.controller.LogisticMapBC;
import com.accenture.challenge.exception.ChallengeException;
import com.accenture.challenge.exception.ResourceNotFoundException;
import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.model.Route;
import com.accenture.challenge.util.Messages;

@RunWith(MockitoJUnitRunner.class)
public class LogisticMapServiceTest {

	@Mock
	private LogisticMapBC controller;

	private LogisticMapService service;

	@Before
	public void setup() {
		this.service = new LogisticMapService(this.controller);
	}

	@Test
	public void whenLoadExistentThenReturn() {
		LogisticMap map = createTestItem();
		Mockito.when(this.controller.load(1l)).thenReturn(map);

		Response response = this.service.getById(1l);
		Object entity = response.getEntity();
		Assert.assertNotNull(entity);
		Assert.assertEquals(map, entity);
	}

	private LogisticMap createTestItem() {
		LogisticMap map = new LogisticMap(1l, "Testmap");
		map.addRoute(new Route("A", "B", 10));
		map.addRoute(new Route("B", "C", 5));		
		return map;
	}

	@Test
	public void whenLoadInexistentThenNotFound() {
		Mockito.when(this.controller.load(2l)).thenThrow(new ResourceNotFoundException(Messages.MAP_NOT_FOUND));

		Response response = this.service.getById(2l);
		Assert.assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void whenSaveValidThenReturnId() {
		LogisticMap map = this.createTestItem();
		Long expected = 1l;

		Mockito.when(this.controller.save(map)).thenReturn(expected);
		Response response = this.service.insert(map);
		Long actual = (Long) response.getEntity();
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void whenSaveInvalidThenBadRequest() {
		LogisticMap item = new LogisticMap();
		Mockito.when(this.controller.save(item)).thenThrow(new ChallengeException(Messages.MAP_NOT_FOUND));
		Response response = this.service.insert(item);
		Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

}
