package com.accenture.challenge.service;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.model.Route;
import com.accenture.challenge.model.dto.PathRequestDTO;
import com.accenture.challenge.model.dto.PathResponseDTO;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;

public class IntegrationPathServiceTest {

	private static final String DEFAULT_PATH = "http://localhost:8080/rest/";
	private static final String MAP_PATH = "map/";
	private static final String PATH = "path";

	private static HttpServer server;

	@BeforeClass
	public static void setup() {
		ResourceConfig resourceConfig = new ResourceConfig();
		resourceConfig.packages("com.accenture.challenge");
		server = GrizzlyHttpServerFactory.createHttpServer(URI.create(DEFAULT_PATH), resourceConfig);	
	}

	public static WebTarget createDefaultWebTarget() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(DEFAULT_PATH);
		webTarget.register(JacksonJaxbJsonProvider.class);
		return webTarget;
	}

	@AfterClass
	public static void tearDown() {
		server.shutdown();
	}

	@Test
	public void whenPathExistentThenPathResponse() {
		this.createMap();
		
		PathRequestDTO pathRequest = this.createExistentPathRequest();
		WebTarget webTarget = createDefaultWebTarget().path(PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(pathRequest));
		Assert.assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		PathResponseDTO pathResponse = response.readEntity(PathResponseDTO.class);
		
		Assert.assertEquals("A B D", pathResponse.getPath());
	}

	@Test
	public void whenPathInvalidThenException() {
		this.createMap();
		
		PathRequestDTO pathRequest = this.createExceptionPathRequest();
		WebTarget webTarget = createDefaultWebTarget().path(PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(pathRequest));
		
		Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	@Test
	public void whenObjectPathRequestInvalidException() {
		PathRequestDTO pathRequest = this.createExceptionPathRequest();
		pathRequest.setOrigin(null);
		pathRequest.setFuelPrice(null);
		WebTarget webTarget = createDefaultWebTarget().path(PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response response = request.post(Entity.json(pathRequest));
		
		Assert.assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
	}

	private PathRequestDTO createExistentPathRequest() {
		PathRequestDTO pathRequestDTO = new PathRequestDTO();
		pathRequestDTO.setOrigin("A");
		pathRequestDTO.setDestination("D");
		pathRequestDTO.setAutonomy(10.0);
		pathRequestDTO.setFuelPrice(2.50);

		return pathRequestDTO;
	}
	
	private PathRequestDTO createExceptionPathRequest() {
		PathRequestDTO pathRequestDTO = new PathRequestDTO();
		pathRequestDTO.setOrigin("A");
		pathRequestDTO.setDestination("F");
		pathRequestDTO.setAutonomy(10.0);
		pathRequestDTO.setFuelPrice(2.50);

		return pathRequestDTO;
	}

	private void createMap() {
		LogisticMap map = new LogisticMap(1l, "Testmap");
		map.addRoute(new Route("A", "B", 10));
		map.addRoute(new Route("B", "D", 15));
		map.addRoute(new Route("A", "C", 20));
		map.addRoute(new Route("C", "D", 30));
		map.addRoute(new Route("B", "E", 50));
		map.addRoute(new Route("D", "E", 30));
		
		WebTarget webTarget = createDefaultWebTarget().path(MAP_PATH);
		Builder request = webTarget.request(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON);
		Response post = request.post(Entity.json(map));
		
		Long newId = post.readEntity(Long.class);
	}
}
