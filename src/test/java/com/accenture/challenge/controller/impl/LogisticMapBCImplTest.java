package com.accenture.challenge.controller.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import com.accenture.challenge.exception.ChallengeException;
import com.accenture.challenge.exception.ResourceNotFoundException;
import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.persistence.LogisticMapDAO;
import com.accenture.challenge.util.Messages;

@RunWith(MockitoJUnitRunner.class)
public class LogisticMapBCImplTest {

	@Mock
	private LogisticMapDAO dao;

	private LogisticMapBCImpl controller;

	@Before
	public void setup() {
		this.controller = new LogisticMapBCImpl(this.dao);
	}

	@Test
	public void whenLoadExistentThenReturn() {
		LogisticMap item = new LogisticMap(1l, "Test");
		Mockito.when(this.dao.load(1l)).thenReturn(item);

		LogisticMap returned = this.controller.load(1l);
		Assert.assertNotNull(returned);
		Assert.assertEquals(item, returned);
	}

	@Test(expected = ResourceNotFoundException.class)
	public void whenLoadInexistentThenThrowException() {
		Mockito.when(this.dao.load(2l)).thenReturn(null);
		this.controller.load(2l);
	}

	@Test
	public void whenSaveValidThenReturn() {
		LogisticMap item = new LogisticMap(1l, "Test");
		Long expected = 1l;

		Mockito.when(this.dao.save(item)).thenReturn(expected);
		Long returned = this.controller.save(item);
		Assert.assertEquals(expected, returned);
	}

	@Test(expected = ChallengeException.class)
	public void whenSaveInvalidThenThrowException() {
		LogisticMap item = new LogisticMap();
		Mockito.when(this.dao.save(item)).thenThrow(new ChallengeException(Messages.MAP_NOT_SAVED));
		Long returned = this.controller.save(item);
		Assert.assertNull(returned);
	}

}
