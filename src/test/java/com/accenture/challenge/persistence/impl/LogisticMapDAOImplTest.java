package com.accenture.challenge.persistence.impl;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.accenture.challenge.model.LogisticMap;
import com.accenture.challenge.persistence.LogisticMapDAO;

public class LogisticMapDAOImplTest {

	private static LogisticMapDAO dao;

	@SuppressWarnings("resource")
	@BeforeClass
	public static void setupBeforeClass() {
		ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");
		dao = context.getBean(LogisticMapDAO.class);
	}

	@Test
	public void whenSaveThenReturnId() {
		LogisticMap map = new LogisticMap(1l, "TestMap");

		Long newId = dao.save(map);
		Assert.assertNotNull(newId);
	}

	@Test
	public void whenLoadInexistentThenReturnNull() {
		LogisticMap pojo = dao.load(10l);
		Assert.assertNull(pojo);
	}

	@Test
	public void whenLoadExistentThenReturn() {
		LogisticMap map = new LogisticMap(1l, "TestMap");

		Long newId = dao.save(map);
		Assert.assertNotNull(newId);

		LogisticMap returned = dao.load(newId);
		Assert.assertNotNull(returned);
	}

}
