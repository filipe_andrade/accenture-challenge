# Accenture Code Java Challenge

##Tecnologias ##
- Core tools:
    - JDK 8
    - Maven 3
- Compile scope:
    - slf4j/logback
    - Spring
    - Hibernate
    - Jersey / JAX-RS
    - Jackson
    - H2 embedded database
- Test scope:
    - JUnit/Hamcrest
    - Mockito
    - PowerMock
    - Spring Test
    - dbUnit


### TEST-SUITE ###
Rodar a suite de testes, na raiz do projeto você pode executar o comando:
mvn test

PS: Na sua IDE é possível executar através dos maven builds.

### Executando ###
Na raiz do projeto, execute
mvn jetty:run

Após inicializado, os serviços estarão acessíveis através das URLS

#Map#
http://localhost:8080/rest/map/{id}- GET - Carrega o mapa do ID específico
http://localhost:8080/rest/map - POST - Insere um novo mapa

#Path
http://localhost:8080/rest/path - POST - Calcula o melhor caminho/custo
JSON {
"origin": "A",
"destination": "B",
"autonomy": 10.0,
"fuelPrice": 3.20
}

Returned JSON {
"path": "A-B",
"cost": 3.20
}

### Considerações ###
1- O problema não explicou como seria feito para decidir qual mapa seria usado ou se todos. No cenário, fiz o sistema pegar apenas o primeiro.
2- O erro exibido no console ( "table not found" ) é um erro do H2 (https://hibernate.atlassian.net/browse/HHH-7002)
3 - Não criei os testes unitários das classes de modelo
4- Fiz para executar no jetty e usando o H2 já que usei um notebook antigo que não tinha nenhum banco instalado. Mas considerando que
para colocar em um banco propriamente dito a mudança seria no dialeto e configurações do datasource apenas. 